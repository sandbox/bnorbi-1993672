
-------------------------------------------------------------------------------
Helponclick Live Chat Software Integration for Drupal 7.x
  by Ronan Dowling, Gorton Studios - ronan (at) gortonstudios (dot) com
-------------------------------------------------------------------------------

DESCRIPTION:
Integrates the Helponclick Live Chat Software to the website.
Use it if you are already or want to be a Helponclick Live Chat Software user.

You can embed your code throught this module, and set the pages where you want
it to use.

-------------------------------------------------------------------------------

INSTALLATION:
* Put the module in your Drupal modules directory and enable it in 
  admin/modules.
* Go to the settings page:
  "Configuration" » "Web services" » "Helponclick settings"
* Insert your code get from your Helponclick Live Chat Software account.
